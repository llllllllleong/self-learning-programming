# Leetcode Practice

This is a repository for learning and practicing programming.
I make a new practice class when my IDE starts to lag, which is usually at
around 4000 lines.

08/24 Progress:

Been doing SQL questions to supplement COMP2400 learning
<br>
<img src="src/src/res/08-24.png" width="400" height="180">

07/24 Progress:
<br>
<img src="src/src/res/07-24.png" width="400" height="180">

06/24 Progress:
<br>
<img src="src/src/res/06-24.png" width="400" height="180">


<br>
05/24 Progress:
<br>
<img src="src/src/res/05-24.png" width="400" height="180">




